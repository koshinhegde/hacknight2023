from os import path
import secrets
import flask
import random
import json

from data_handler import DataHandler


app: flask.Flask = flask.Flask(__name__.split(".")[0])
app.secret_key = secrets.token_hex(16)


class RestApi:
    @app.get("/")
    @staticmethod
    def handle_index_file() -> flask.Response:
        return flask.send_file(path.join(
            path.curdir,
            "..",
            "frontend",
            "html",
            "index.html"
        ))

    @staticmethod
    @app.get("/<path:file_path>")
    def handle_get(file_path: str) -> flask.Response:
        return flask.send_file(path.join(path.curdir, "..", "frontend", file_path))

    @staticmethod
    @app.post("/login")
    def handle_login() -> flask.Response:
        response: flask.Response = flask.Response()
        data: dict = dict(flask.request.form)

        if "user_name" not in data or "password" not in data:
            response.status_code = 400
            return response

        try:
            DataHandler.auth_user(data["user_name"], data["password"])
        except ValueError as value_error:
            response.status_code = 403
            response.data = str(value_error)
            return response

        response.status_code = 200
        flask.session[data["user_name"]] = secrets.token_urlsafe(16)
        response.set_cookie("token", flask.session[data["user_name"]])

        return response

    @staticmethod
    @app.post("/add_user")
    def add_user() -> flask.Response:
        response: flask.Response = flask.Response()
        data: dict = dict(flask.request.form)

        if "user_name" not in data or "password" not in data:
            response.status_code = 400
            return response

        try:
            DataHandler.add_user(data["user_name"], data["password"])
        except ValueError as value_error:
            response.status_code = 403
            response.data = str(value_error)
            return response

        response.status = 200

        return response
    
    @staticmethod
    @app.post("/questions")
    def get_questions() -> flask.Response:
        response: flask.Response = flask.Response()
        data: dict = dict(flask.request.form)

        if "level" not in data or "count" not in data:
            response.status_code = 400
            return response
        if not data["level"].isnumeric() or not data["count"].isnumeric():
            response.status_code = 400
            return response

        level = int(data["level"])
        count = int(data["count"])
        questions: list = DataHandler.get_questions(level)

        if len(questions) < count:
            flask.abort(403)

        response_data = [
                questions.pop(random.randrange(0, len(questions))) \
                        for _ in range(count)
        ]
        response.data = json.dumps(response_data)

        return response
