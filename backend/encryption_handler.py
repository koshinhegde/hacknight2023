from hashlib import sha3_512
import bcrypt


class EncryptionHandler:
    @staticmethod
    def get_encrypted(password: str) -> bytes:
        hashed_password: bytes = sha3_512(password.encode("utf-8")).digest()
        return bcrypt.hashpw(hashed_password, bcrypt.gensalt())

    @staticmethod
    def check_password(password: str, encrypted_password: bytes) -> bool:
        hashed_password: bytes = sha3_512(password.encode("utf-8")).digest()
        return bcrypt.checkpw(hashed_password, encrypted_password)
