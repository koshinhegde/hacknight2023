import json
import re
import mysql.connector


connection = mysql.connector.connect(
        host="localhost",
        user="kosh",
        password="temp",
        database="quiz"
)
cursor = connection.cursor()

data : dict = {}

def drop_table():
    cursor.execute("DROP TABLE `questions`;")


def create_table():
    cursor.execute("CREATE TABLE questions( id INT PRIMARY KEY AUTO_INCREMENT, question VARCHAR(255), difficulty INT, options JSON, answer INT, explanation VARCHAR(255) );")


def append_values():
    with open("./questions.json", "r", encoding="utf-8") as file:
        data = json.load(file)

    for question in data:
        command: str = \
                "INSERT INTO `questions` " + \
                "(question, difficulty, options, answer, explanation) " + \
                "VALUES (" + \
                f"\"{question['Question']}\", " + \
                f"{question['Difficulty']}, " + \
                "\'" + json.dumps(question["Options"]).replace("'", "\\'") + "\', " + \
                f"{question['Answer']}, " + \
                f"\"{question['Explanation']}\"" + \
                ");"
        print(command)
        cursor.execute(command)
connection.commit()
connection.close()
with open("./questions.json", "r", encoding="utf-8") as file:
    data = json.load(file)
for a in data:
    a["Options"] = [c[3:] for c in a["Options"]]
with open("./questions2.json", "w") as f:
    json.dump(data, f)
