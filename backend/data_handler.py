import mysql.connector
from mysql.connector import cursor
from encryption_handler import EncryptionHandler


class DataHandler:
    __database_connect = mysql.connector.connect(
                    host="localhost",
                    user="kosh",
                    password="temp",
                    database="quiz"
    )
    __database_cursor = __database_connect.cursor()

    @staticmethod
    def auth_user(user_name: str, password: str) -> None:
        """
        Throw an error if user info is wrong
        """
        command: str = f"SELECT * FROM user_auth WHERE user_name='{user_name}';"
        DataHandler.__database_cursor.execute(command)
        data = DataHandler.__database_cursor.fetchall()
        if len(data) == 0:
            raise ValueError(f"No user with name {user_name}")
        data = bytes(data)[0][1]
        if not EncryptionHandler.check_password(password, data[2:-1]):
            raise ValueError(f"Incorrect password for {user_name}")

    @staticmethod
    def add_user(user_name: str, password: str) -> None:
        """
        Save the user on the database
        """
        encrypted_password: bytes = EncryptionHandler.get_encrypted(password)
        command: str = \
                "INSERT INTO `user_auth` (user_name, password) " + \
                f'VALUES ("{user_name}", "{encrypted_password}")'
        try:
            DataHandler.__database_cursor.execute(command)
        except mysql.connector.errors.IntegrityError as error:
            raise ValueError(f"User with the name {user_name} already exists") from error
        DataHandler.__database_connect.commit()

    @staticmethod
    def get_questions(level: int):
        command: str = f"SELECT * from questions WHERE difficulty={level}"
        DataHandler.__database_cursor.execute(command)
        return DataHandler.__database_cursor.fetchall()
